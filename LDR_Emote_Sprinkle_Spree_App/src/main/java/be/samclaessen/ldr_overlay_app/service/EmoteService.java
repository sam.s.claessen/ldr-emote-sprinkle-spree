package be.samclaessen.ldr_overlay_app.service;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public interface EmoteService {

    void sendMessage(String id, EmojiMessage msg);

    void subscribe(String id, Consumer<EmojiMessage> handleMessage);

    String getId();
}
