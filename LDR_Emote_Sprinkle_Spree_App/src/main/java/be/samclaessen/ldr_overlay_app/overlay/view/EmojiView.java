package be.samclaessen.ldr_overlay_app.overlay.view;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

public class EmojiView {
    public EmojiType type;
    public ImageView img;
    public Circle background;
    public double x;
    public double y;

    public EmojiView(EmojiType type, double x, double y, double size) {
        this.img = new ImageView(new Image(type.getImage(), size, size, false, false));
        this.img.setX(x);
        this.img.setY(y);
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public EmojiView(EmojiType type, double x, double y, double size, Circle background) {
        this.img = new ImageView(new Image(type.getImage(), size, size, false, false));
        this.img.setX(x);
        this.img.setY(y);
        this.x = x;
        this.y = y;
        this.type = type;
        this.background = background;
    }
}
