package be.samclaessen.ldr_overlay_app;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdrOverlayAppApplication {

    public static void main(String[] args) {
        Application.launch(LdrOverlayJavafxAppApplication.class, args);
    }
}
