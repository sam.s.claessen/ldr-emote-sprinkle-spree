package be.samclaessen.ldr_overlay_app.overlay.controller;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import be.samclaessen.ldr_overlay_app.overlay.view.EmojiView;
import be.samclaessen.ldr_overlay_app.overlay.view.OverlayView;
import be.samclaessen.ldr_overlay_app.service.EmoteService;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
@Getter
@Setter
public class OverlayController {
    private String id;
    private String target;

    private final Logger LOGGER = LoggerFactory.getLogger(OverlayController.class);
    private final EmoteService emoteService;
    private final Random generator;
    private double maxX;
    private double maxY;

    private final List<EmojiView> emojis;

    private boolean settingsVisible;
    private boolean visible = true;

    public OverlayController(EmoteService emoteService, Random generator) {
        this.emoteService = emoteService;
        this.generator = generator;
        this.emojis = new ArrayList<>();
    }

    public void getIds() throws IOException {
        try {
            this.readFile();
            if (id == null) {
                this.id = emoteService.getId();
                this.writeFile();
            }
            if (target != null) {
                subscribe();
                settingsVisible = false;
            }
        } catch (FileNotFoundException ignored) {

        }
    }

    public void writeFile() throws IOException {
        List<String> settings = new ArrayList<>();
        if (this.id != null) {
            settings.add("id=" + this.id);
        }
        if (this.target != null) {
            settings.add("target=" + this.target);
        }

        String settingsLine = String.join(",", settings);

        File settingsFile = new File(new FileSystemResource("").getFile().getAbsolutePath() + "/settings.txt");
        boolean newFile = settingsFile.createNewFile();
        if (newFile){
            LOGGER.info("Created the settings.txt file to save the id and target");
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(settingsFile));
        writer.write(settingsLine);
        writer.close();
    }

    public void readFile() {
        try {
            File settingsFile = new File(new FileSystemResource("").getFile().getAbsolutePath() + "/settings.txt");
            if (settingsFile.exists()){
                InputStream is = settingsFile.toURI().toURL().openStream();
                if (is != null) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String settingsLine = reader.readLine();
                    if (settingsLine != null) {
                        String[] settings = settingsLine.split(",");
                        for (String setting : settings) {
                            String[] settingValues = setting.split("=");
                            switch (settingValues[0]) {
                                case "id" -> this.id = settingValues[1];
                                case "target" -> this.target = settingValues[1];
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error("De settings file kon niet worden uitgelezen");
        }
    }

    public void subscribe() {
        emoteService.subscribe(id, (msg)->{
            double x = generator.nextDouble() * this.maxX;
            double y = this.maxY;

            this.emojis.add(new EmojiView(msg.getType(), x, y, 50));
        });

    }

    public void calculateBounds() {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        this.maxX = primScreenBounds.getMaxX();
        this.maxY = primScreenBounds.getMaxY();
    }

    public void addEmoji(EmojiView emoji) {
        this.emojis.add(emoji);
    }

    public List<EmojiView> animateEmojis(double pixels) {
        List<EmojiView> toRemove = new ArrayList<>();
        for (EmojiView emoji : emojis) {
            emoji.y -= pixels;
            emoji.img.setY(emoji.y);
            emoji.img.setX(emoji.x);
            if (emoji.y < -OverlayView.EMOJI_SIZE) {
                toRemove.add(emoji);
            }
        }

        for (EmojiView animatedEmoji : toRemove) {
            emojis.remove(animatedEmoji);
        }
        return toRemove;
    }

    public void toggleSettingsVisible() {
        this.settingsVisible = !settingsVisible;
    }

    public void toggleVisible() {
        this.visible = !visible;
    }

    public void sendMessage(EmojiMessage emojiMessage) {
        this.emoteService.sendMessage(this.target, emojiMessage);
    }
}
