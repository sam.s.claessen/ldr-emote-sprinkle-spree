package be.samclaessen.ldr_overlay_app.service.reactive;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import be.samclaessen.ldr_overlay_app.overlay.model.EmojiType;
import javafx.collections.ObservableList;

import java.util.function.Consumer;

import static javafx.collections.FXCollections.observableArrayList;

public class EmojiSubscriber implements Consumer<EmojiMessage> {
    ObservableList<EmojiMessage> emojiMessages = observableArrayList();

    public EmojiSubscriber(String id, WebClientEmojiClient webClient) {
        webClient.emojisFor(id).subscribe(this);
    }

    @Override
    public void accept(EmojiMessage emojiMessage) {
        if (emojiMessage.getType() != EmojiType.NULL) {
            this.emojiMessages.addAll(emojiMessage);
        }
    }

    public ObservableList<EmojiMessage> getEmojiMessages() {
        return emojiMessages;
    }
}
