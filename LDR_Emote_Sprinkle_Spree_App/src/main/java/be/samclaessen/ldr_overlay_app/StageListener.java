package be.samclaessen.ldr_overlay_app;

import be.samclaessen.ldr_overlay_app.LdrOverlayJavafxAppApplication.StageReadyEvent;
import be.samclaessen.ldr_overlay_app.overlay.view.OverlayView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.IOException;

@Component
public class StageListener implements ApplicationListener<StageReadyEvent> {

    private final OverlayView overlayView;

    public StageListener(OverlayView overlayView) {
        this.overlayView = overlayView;
    }

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        Stage stage = event.getStage();
        stage.initStyle(StageStyle.UTILITY);
        stage.setOpacity(0);
        stage.show();
        try {
            Stage secondaryStage = new Stage();
            secondaryStage.initOwner(stage);
            overlayView.setStage(secondaryStage);
            overlayView.startView();
        } catch (IOException | AWTException e) {
            throw new RuntimeException(e);
        }
    }
}
