package be.samclaessen.ldr_overlay_app.overlay.view;

import be.samclaessen.ldr_overlay_app.LdrOverlayJavafxAppApplication;
import be.samclaessen.ldr_overlay_app.overlay.controller.OverlayController;
import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import be.samclaessen.ldr_overlay_app.overlay.model.EmojiType;
import be.samclaessen.ldr_overlay_app.service.EmoteService;
import javafx.animation.AnimationTimer;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.io.*;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class OverlayView {
    //region Javafx
    LdrOverlayJavafxAppApplication ldrOverlayJavafxAppApplication;
    private Stage stage;
    private final Group root;

    private Scene scene;
    //endregion

    //region EmoteMenu
    private List<EmojiView> emojiButtons;
    private Button toggleSettingsButton;
    private Button hideButton;
    private Rectangle background;

    //region Settings
    private Label targetLabel;
    private TextArea targetText;
    private Button applyButton;

    private Label idLabel;
    private TextArea idText;
    private Button copyToClipboardButton;
    //endregion Settings

    //endregion EmoteMenu

    //region SystemTray
    private TrayIcon trayIcon;
    private SystemTray tray;
    private PopupMenu menu;
    private MenuItem closeItem;
    //endregion


    //region Layout params
    double startX;
    double startY;

    private final int OFFSET_SIZE = 5;
    private final int ROW_SIZE = 10;
    private final int MARGIN = 60;
    private final double BUTTON_SIZE = 30.0;

    public static final double EMOJI_SIZE = 50;

    private final double SPEED = 400;

    private static final double PADDING = 20;
    //endregion

    private final OverlayController controller;


    private final Random generator = new Random();

    public OverlayView(LdrOverlayJavafxAppApplication app, OverlayController controller) {
        this.controller = controller;
        this.root = new Group();
        this.ldrOverlayJavafxAppApplication = app;
    }

    public void startView() throws IOException, AWTException {
        launchTray();
        setBounds();
        initComponents();
        layoutComponents();
        fillIds();
        addEventHandlers();
        startScene();
        startAnimation();
    }

    private void launchTray() throws AWTException, IOException {
        System.setProperty("java.awt.headless", "false");

        URL url = new ClassPathResource("emote_images/smiling_hearts_icon.png").getURL();
        Image image = ImageIO.read(url);

        trayIcon = new TrayIcon(image, "emote sprinkle spree");
        tray = SystemTray.getSystemTray();

        menu = new PopupMenu();
        closeItem = new MenuItem("close");

        closeItem.addActionListener(e -> {
            try {
                tray.remove(trayIcon);
                ldrOverlayJavafxAppApplication.stop();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
        menu.add(closeItem);

        trayIcon.setPopupMenu(menu);

        tray.add(trayIcon);
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent e) {
                if (e.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    toggleVisible();
                }
            }
        });
    }

    private void toggleVisible() {
        this.controller.toggleVisible();
        if (!this.controller.isVisible()) {
            showSettings(false);
        } else {
            showSettings(this.controller.isSettingsVisible());
        }
        showEmojis();
    }

    private void initComponents() {
        targetLabel = new Label("Target");
        addChild(targetLabel);
        targetText = new TextArea("");
        this.addChild(targetText);

        idLabel = new Label("id");
        addChild(idLabel);
        idText = new TextArea(this.controller.getId());
        this.addChild(idText);

        applyButton = new Button("Apply");
        this.addChild(applyButton);

        copyToClipboardButton = new Button("copy");
        this.addChild(copyToClipboardButton);

        toggleSettingsButton = new Button("settings");
        this.addChild(toggleSettingsButton);

        hideButton = new Button("hide");
        this.addChild(hideButton);

        emojiButtons = new ArrayList<>();

        background = new Rectangle();
        addChild(background);

        int i = 0;
        for (EmojiType value : EmojiType.values()) {
            if (value != EmojiType.NULL) {

                double offsetX = (i % ROW_SIZE) * (OFFSET_SIZE + BUTTON_SIZE);
                double offsetY = ((int) (i / ROW_SIZE)) * (OFFSET_SIZE + BUTTON_SIZE);
                double buttonY = startY - offsetY;
                double buttonX = startX - offsetX;

                Circle background = new Circle(buttonX + BUTTON_SIZE / 2, buttonY + BUTTON_SIZE / 2, BUTTON_SIZE / 2);
                EmojiView emojiButton = new EmojiView(value, buttonX, buttonY, BUTTON_SIZE, background);

                root.getChildren().add(emojiButton.background);
                root.getChildren().add(emojiButton.img);
                emojiButtons.add(emojiButton);
                i++;
            }
        }
    }

    private void layoutComponents() {
        double emojiSize = EmojiType.values().length;
        double backgroundWidth = (ROW_SIZE) * (OFFSET_SIZE + BUTTON_SIZE) + (PADDING * 2);
        double backgroundHeight = (Math.round(emojiSize / ROW_SIZE + .5)) * (OFFSET_SIZE + BUTTON_SIZE) + (PADDING * 2);
        double backgroundX = startX - backgroundWidth + BUTTON_SIZE + PADDING;
        double backgroundY = startY - backgroundHeight + BUTTON_SIZE + PADDING;

        double settingsMargin = 10;
        double settingsHeight = 40;
        double settingswidth = 50;

        idLabel.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        idLabel.setLayoutX(backgroundX);
        idLabel.setLayoutY(backgroundY - settingsMargin - settingsHeight);
        idLabel.setMinWidth(settingswidth);
        idLabel.setMinHeight(settingsHeight);
        idLabel.setAlignment(Pos.CENTER);
        idLabel.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, BorderWidths.DEFAULT)));

        idText.setLayoutX(backgroundX + settingswidth);
        idText.setLayoutY(backgroundY - settingsMargin - settingsHeight);
        idText.setMaxWidth(backgroundWidth - (settingswidth * 2));
        idText.setMinWidth(backgroundWidth - (settingswidth * 2));
        idText.setMaxHeight(settingsHeight);
        idText.setMinHeight(settingsHeight);
        idText.setEditable(false);

        copyToClipboardButton.setLayoutX(backgroundX + backgroundWidth - settingswidth);
        copyToClipboardButton.setLayoutY(backgroundY - settingsMargin - settingsHeight);
        copyToClipboardButton.setMinWidth(settingswidth);
        copyToClipboardButton.setMaxWidth(settingswidth);
        copyToClipboardButton.setMaxHeight(settingsHeight);
        copyToClipboardButton.setMinHeight(settingsHeight);

        targetLabel.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        targetLabel.setLayoutX(backgroundX);
        targetLabel.setLayoutY(backgroundY - settingsMargin - (settingsHeight * 2));
        targetLabel.setMinWidth(settingswidth);
        targetLabel.setMinHeight(settingsHeight);
        targetLabel.setAlignment(Pos.CENTER);
        targetLabel.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, BorderWidths.DEFAULT)));


        targetText.setLayoutX(backgroundX + settingswidth);
        targetText.setLayoutY(backgroundY - settingsMargin - (settingsHeight * 2));
        targetText.setMaxWidth(backgroundWidth - 50 - 50);
        targetText.setMaxHeight(settingsHeight);
        targetText.setMinHeight(settingsHeight);

        applyButton.setLayoutX(backgroundX + backgroundWidth - settingswidth);
        applyButton.setLayoutY(backgroundY - settingsMargin - (settingsHeight * 2));
        applyButton.setMinWidth(settingswidth);
        applyButton.setMinHeight(settingsHeight);
        applyButton.setMaxHeight(settingsHeight);

        toggleSettingsButton.setLayoutY(this.controller.getMaxY() - 25);
        toggleSettingsButton.setLayoutX(this.controller.getMaxX() - 75 - 75);
        toggleSettingsButton.setMaxWidth(75);
        toggleSettingsButton.setMinWidth(75);
        toggleSettingsButton.setMaxHeight(25);
        toggleSettingsButton.setMinHeight(25);

        hideButton.setLayoutX(this.controller.getMaxX() - 75);
        hideButton.setLayoutY(this.controller.getMaxY() - 25);
        hideButton.setMaxWidth(75);
        hideButton.setMinWidth(75);
        hideButton.setMaxHeight(25);
        hideButton.setMinHeight(25);

        background.setX(backgroundX);
        background.setY(backgroundY);
        background.setWidth(backgroundWidth);
        background.setHeight(backgroundHeight);
        background.setFill(Color.BLACK);
        background.setOpacity(.6);
    }

    private void fillIds() throws IOException {
        this.controller.getIds();
        this.idText.setText(this.controller.getId());
        this.targetText.setText(this.controller.getTarget());
        showSettings(this.controller.isSettingsVisible());
    }

    private void addEventHandlers() {
        applyButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            this.controller.setTarget(targetText.getText());
            this.controller.subscribe();
        });

        copyToClipboardButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent content = new ClipboardContent();
            content.putString(this.controller.getId());
            clipboard.setContent(content);
        });

        toggleSettingsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            this.controller.toggleSettingsVisible();
            showSettings(this.controller.isSettingsVisible());
        });

        hideButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            toggleVisible();
        });

        for (EmojiView emojiButton : emojiButtons) {
            emojiButton.img.addEventHandler(MouseEvent.MOUSE_CLICKED, (mouseEventEmoji -> {
                doEmojiButtonAction(emojiButton);
            }));

            emojiButton.background.addEventHandler(MouseEvent.MOUSE_CLICKED, (mouseEventEmoji -> {
                doEmojiButtonAction(emojiButton);
            }));
        }
    }

    private void showSettings(boolean show) {
        idLabel.setVisible(show);
        idText.setVisible(show);
        copyToClipboardButton.setVisible(show);

        targetLabel.setVisible(show);
        targetText.setVisible(show);
        applyButton.setVisible(show);
    }

    private void showEmojis() {
        for (EmojiView emojiButton : emojiButtons) {
            emojiButton.img.setVisible(this.controller.isVisible());
            emojiButton.background.setVisible(this.controller.isVisible());
        }
        hideButton.setVisible(this.controller.isVisible());
        background.setVisible(this.controller.isVisible());
        toggleSettingsButton.setVisible(this.controller.isVisible());
    }

    private void doEmojiButtonAction(EmojiView emojiButton) {
        double x = (generator.nextDouble() * (this.controller.getMaxX() - EMOJI_SIZE * 2)) + EMOJI_SIZE;
        double y = this.controller.getMaxY();
        EmojiView view = new EmojiView(emojiButton.type, x, y, EMOJI_SIZE);
        this.controller.addEmoji(view);
        EmojiMessage emojiMessage = new EmojiMessage(emojiButton.type, LocalDateTime.now());
        this.controller.sendMessage(emojiMessage);
    }

    private void startScene() {
        scene = new Scene(root, Color.TRANSPARENT);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setScene(scene);
        stage.show();
        stage.setAlwaysOnTop(true);
        scene.setRoot(root);
        scene.getRoot().setStyle("-fx-background-color: transparent");
    }

    private void startAnimation() {
        AnimationTimer loop = new AnimationTimer() {
            long last = System.currentTimeMillis();

            @Override
            public void handle(long now) {
                long milliNow = System.currentTimeMillis();
                long diff = milliNow - last;
                last = milliNow;
                double seconds = diff / 1000.0;
                double pixels = seconds * SPEED;

                animateEmojis(pixels);

                stage.setScene(scene);
            }
        };
        loop.start();
    }

    private void addChild(Node node) {
        root.getChildren().add(node);
    }

    private void setBounds() {
        this.controller.calculateBounds();
        stage.setWidth(this.controller.getMaxX());
        stage.setHeight(this.controller.getMaxY());
        stage.setX(0);
        stage.setY(0);

        startX = this.controller.getMaxX() - MARGIN - PADDING;
        startY = this.controller.getMaxY() - MARGIN - PADDING;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private void animateEmojis(double pixels) {
        for (EmojiView emoji : this.controller.getEmojis()) {
            if (!root.getChildren().contains(emoji.img)) {
                root.getChildren().add(emoji.img);
            }
        }
        List<EmojiView> emojisToRemove= this.controller.animateEmojis(pixels);

        for (EmojiView toRemove : emojisToRemove) {
            this.root.getChildren().remove(toRemove);
        }
    }
}
