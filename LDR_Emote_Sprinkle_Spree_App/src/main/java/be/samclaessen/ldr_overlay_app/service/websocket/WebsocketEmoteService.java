package be.samclaessen.ldr_overlay_app.service.websocket;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import be.samclaessen.ldr_overlay_app.service.EmoteService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.Objects;
import java.util.function.Consumer;

@Service
@ConditionalOnProperty(value = "emote.service", havingValue = "websocket")
public class WebsocketEmoteService implements EmoteService {
    private final RestTemplate template;
    private final WebSocketStompClient stompClient;

    @Value("${emote_sprinkle_spree.url}")
    private String serverUrl;

    @Value("${emote_sprinkle_spree.url.websocket}")
    private String websocketUrl;

    public WebsocketEmoteService(RestTemplate template, WebSocketStompClient stompClient) {
        this.template = template;
        this.stompClient = stompClient;
    }

    @Override
    public void sendMessage(String target, EmojiMessage msg) {
        template.postForEntity(serverUrl + "/emotes/" + target, msg, EmojiMessage.class);
    }

    @Override
    public void subscribe(String id, Consumer<EmojiMessage> handleMessage) {
        StompSessionHandler sessionHandler = new EmoteStompSessionHandler(handleMessage, id);
        stompClient.connectAsync(websocketUrl, sessionHandler);
    }

    @Override
    public String getId() {
        return template.getForEntity(serverUrl + "/id", String.class).getBody();
    }
}
