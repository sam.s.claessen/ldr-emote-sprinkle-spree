package be.samclaessen.ldr_overlay_server.model;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@JsonDeserialize
@JsonSerialize
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmojiMessage {

    private String type;
    private LocalDateTime timeSent;
}
