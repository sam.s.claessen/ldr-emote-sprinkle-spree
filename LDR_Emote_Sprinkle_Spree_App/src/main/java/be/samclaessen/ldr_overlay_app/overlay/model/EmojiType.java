package be.samclaessen.ldr_overlay_app.overlay.model;

public enum EmojiType {
    NULL(""),
    SMILING_HEARTS("emote_images/smiling_hearts.png"),
    HEART_EYES("emote_images/heart_eyes.png"),
    KISSING_BLUSH("emote_images/kissing_blush.png"),
    KISSING_HEART("emote_images/kissing_heart.png"),
    BLUSH("emote_images/blush.png"),
    HUGGING_FACE("emote_images/hugging_face.png"),
    PARTY("emote_images/party.png"),
    STAR_EYES("emote_images/star_eyes.png"),
    YUM("emote_images/yum.png"),
    THINKING("emote_images/thinking.png"),
    ROFL("emote_images/rofl.png"),
    JOY("emote_images/joy.png"),
    LAUGHING("emote_images/laughing.png"),
    GRIN("emote_images/grinning.png"),
    SMILE("emote_images/smile.png"),
    SWEATING("emote_images/sweating.png"),
    PLEADING("emote_images/pleading.png"),
    WEARY("emote_images/weary.png"),
    SCREAMING("emote_images/screaming.png"),
    OPEN_MOUTH("emote_images/open_mouth.png"),
    ANGRY("emote_images/angry.png"),
    SMILING_TEAR("emote_images/smiling_tear.png"),
    SAD("emote_images/sad.png"),
    SOB("emote_images/sob.png"),
    HEART("emote_images/heart.png"),
    THUMBS_UP("emote_images/thumbs_up.png"),
    THUMBS_DOWN("emote_images/thumbs_down.png"),
    MUSCLE("emote_images/muscle.png"),
    FIRE("emote_images/fire.png"),
    STAR("emote_images/star.png"),
    MUSIC_NOTE("emote_images/music_note.png"),
    HUNDRED("emote_images/hundred.png"),
    EXCLAMATION_MARK("emote_images/exclamation_mark.png"),
    QUESTION_MARK("emote_images/question_mark.png"),
    SPEECH_BALLOON("emote_images/speech_balloon.png"),
    PHONE_ARROW("emote_images/phone_arrow.png"),
    MAIL_ARROW("emote_images/mail_arrow.png"),
    BELL("emote_images/bell.png"),
    WAVE("emote_images/wave.png"),
    ;

    private final String image;

    EmojiType(String image) {
        this.image = image;
    }

    public String getImage() {
        return this.image;
    }
}
