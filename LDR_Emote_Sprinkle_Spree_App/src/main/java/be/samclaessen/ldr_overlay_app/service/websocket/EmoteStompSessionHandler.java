package be.samclaessen.ldr_overlay_app.service.websocket;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;

import java.lang.reflect.Type;
import java.util.function.Consumer;

public class EmoteStompSessionHandler implements StompSessionHandler {
    private final Logger logger = LoggerFactory.getLogger(EmoteStompSessionHandler.class);

    private final Consumer<EmojiMessage> handleMessage;
    private final String id;

    public EmoteStompSessionHandler(Consumer<EmojiMessage> handleMessage, String id) {
        this.handleMessage = handleMessage;
        this.id = id;
    }

    @Override
    public void afterConnected(StompSession session, @Nullable StompHeaders connectedHeaders) {
        logger.info("New session established : " + session.getSessionId());
        session.subscribe("/topic/emotes/" + id, this);
        logger.info("Subscribed to /topic/emotes");
    }

    @Override
    public void handleException(@Nullable StompSession session, StompCommand command, @Nullable StompHeaders headers, @Nullable byte[] payload, @Nullable Throwable exception) {
        logger.error("Got an exception: ", exception);
    }

    @Override
    public void handleTransportError(@Nullable StompSession session, @Nullable Throwable exception) {
        logger.error("Couldn't send message: ", exception);
    }

    @Override
    @Nonnull
    public Type getPayloadType(@Nullable StompHeaders headers) {
        return EmojiMessage.class;
    }

    @Override
    public void handleFrame(@Nullable StompHeaders headers, Object payload) {
        handleMessage.accept((EmojiMessage) payload);
    }

}
