package be.samclaessen.ldr_overlay_server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static javafx.collections.FXCollections.observableArrayList;

@RestController
public class EmojiController {

    @GetMapping(value = "id")
    private String getId() {
        return UUID.randomUUID().toString();
    }
}
