package be.samclaessen.ldr_overlay_app.service.reactive;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import be.samclaessen.ldr_overlay_app.service.EmoteService;
import javafx.collections.ListChangeListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.function.Consumer;

@Service
@ConditionalOnProperty(value = "emote.service", havingValue = "reactive")
public class ReactiveEmoteService implements EmoteService {

    private EmojiSubscriber emojiSubscriber;
    private final WebClientEmojiClient webClientEmojiClient;
    private final RestTemplate template;

    @Value("${emote_sprinkle_spree.url}")
    private String serverUrl;

    public ReactiveEmoteService(WebClientEmojiClient webClientEmojiClient, RestTemplate template) {
        this.webClientEmojiClient = webClientEmojiClient;
        this.template = template;
    }

    @Override
    public void sendMessage(String id, EmojiMessage msg) {
        template.postForEntity(serverUrl + "/emojis/" + id, msg, EmojiMessage.class);
    }

    @Override
    public void subscribe(String id, Consumer<EmojiMessage> handleMessage) {
        emojiSubscriber = new EmojiSubscriber(id, this.webClientEmojiClient);
        emojiSubscriber.getEmojiMessages().addListener((ListChangeListener.Change<? extends EmojiMessage> change) -> {

            while (change.next()) {
                if (change.wasAdded()) {
                    for (EmojiMessage message : change.getAddedSubList()) {
                        handleMessage.accept(message);
                        emojiSubscriber.getEmojiMessages().remove(message);
                    }
                }
            }
        });
    }

    @Override
    public String getId() {
        return template.getForEntity(serverUrl + "/id", String.class).getBody();
    }
}
