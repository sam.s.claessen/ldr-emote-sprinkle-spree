package be.samclaessen.ldr_overlay_server.controller.websocket;

import be.samclaessen.ldr_overlay_server.model.EmojiMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.time.format.DateTimeFormatter;

@RestController
public class EmojiWebsocketController {
    private final SimpMessagingTemplate template;
    private final Logger LOGGER = LoggerFactory.getLogger(EmojiWebsocketController.class);

    public EmojiWebsocketController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @PostMapping("/emotes/{id}")
    public EmojiMessage sendEmojiMessage(@RequestBody EmojiMessage message, @PathVariable String id) {
        logMessage(message, id);
        template.convertAndSend("/topic/emotes/" + id, message);
        return message;
    }

    @MessageMapping("/emotes/{id}")
    @SendTo("/topic/emotes/{id}")
    public EmojiMessage emojiMessages(@DestinationVariable("id") String id, @Payload EmojiMessage message) {
        logMessage(message, id);
        return message;
    }

    private void logMessage(EmojiMessage emojiMessage, String id) {
        LOGGER.info("%s: Sent a websocket message %s to %s".formatted(emojiMessage.getTimeSent().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), emojiMessage.getType(), id));
    }
}
