package be.samclaessen.ldr_overlay_server.controller.reactive;

import be.samclaessen.ldr_overlay_server.model.EmojiMessage;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static javafx.collections.FXCollections.observableArrayList;


@RestController
public class EmojiReactiveController {
    private final Logger LOGGER = LoggerFactory.getLogger(EmojiReactiveController.class);
    private final ConcurrentHashMap<String, ObservableList<EmojiMessage>> emojis = new ConcurrentHashMap<>();

    public EmojiReactiveController() {
    }

    @GetMapping(value = "/emojis/{id}",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    private Flux<EmojiMessage> emojis(@PathVariable String id) {
        checkExists(id);
        return Flux.interval(Duration.ofMillis(100)).map((r) -> {
            if (!emojis.get(id).isEmpty()) {
                EmojiMessage emojiMessage = emojis.get(id).get(0);
                emojis.get(id).remove(emojiMessage);
                return emojiMessage;
            }
            return new EmojiMessage("NULL", LocalDateTime.now());
        });
    }

    private void checkExists(String id) {
        if (!this.emojis.containsKey(id)) {
            this.emojis.put(id, observableArrayList());
        }
    }

    @PostMapping(value = "/emojis/{target}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    private void sendEmoji(@RequestBody EmojiMessage message, @PathVariable String target) {
        logMessage(message, target);
        checkExists(target);
        emojis.get(target).add(message);
    }

    private void logMessage(EmojiMessage emojiMessage, String id) {
        LOGGER.info("%s: Sent a reactive message %s to %s".formatted(emojiMessage.getTimeSent().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), emojiMessage.getType(), id));
    }
}
