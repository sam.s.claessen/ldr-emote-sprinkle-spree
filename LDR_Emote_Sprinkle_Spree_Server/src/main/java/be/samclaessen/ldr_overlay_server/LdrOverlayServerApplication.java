package be.samclaessen.ldr_overlay_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdrOverlayServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(LdrOverlayServerApplication.class, args);
	}
}
