package be.samclaessen.ldr_overlay_app.service.reactive;

import be.samclaessen.ldr_overlay_app.overlay.model.EmojiMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.io.IOException;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static reactor.util.retry.Retry.*;

@Component
public class WebClientEmojiClient {

    private final WebClient webClient;
    private Logger LOGGER = LoggerFactory.getLogger(WebClientEmojiClient.class);

    @Value("${emote_sprinkle_spree.url}")
    private String serverUrl;

    public WebClientEmojiClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public Flux<EmojiMessage> emojisFor(String id) {
        return webClient.get().uri(this.serverUrl + "/emojis/" + id)
                .retrieve()
                .bodyToFlux(EmojiMessage.class)
                .retryWhen(backoff(5, ofMillis(100)).maxBackoff(ofSeconds(20)))
                .doOnError(IOException.class, e -> LOGGER.error("Couldn't connect to webclient with error: " + e.getMessage()));
    }


}
